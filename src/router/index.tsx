import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Header from "~/components/Header";
import Detail from "~/page/PagePrivate/Detail";
import Home from "~/page/PagePrivate/Home";
import Product from "~/page/PagePrivate/Product";
import ForgotPassword from "~/page/PagePublic/ForgotPassword";
import Login from "~/page/PagePublic/Login";
import Private from "./Private";
import Public from "./Public";

function RouterLogin() {
  return (
    <>
      <BrowserRouter>
        <Header />

        <Routes>
          <Route path="/" element={<Private />}>
            <Route path="/home" element={<Home />} />
            <Route path="/detail" element={<Detail />} />
            <Route path="/product" element={<Product />} />
          </Route>

          <Route path="/" element={<Public />}>
            <Route index path="/login" element={<Login />} />
            <Route index path="/forgotpassword" element={<ForgotPassword />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default RouterLogin;
