import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";

function Public() {
  const loggedInUser = useSelector((state: any) => state.user.current);
  const isLoggedIn = !!loggedInUser.id;
  return isLoggedIn ? <Navigate to="/home" replace /> : <Outlet />;
}

export default Public;
