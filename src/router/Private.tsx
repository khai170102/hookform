import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";

interface Iprops {
  Component: Function;
}
function Private({ children }: any) {
  const loggedInUser = useSelector((state: any) => state.user.current);
  const isLoggedIn = !!loggedInUser.id;
  return isLoggedIn ? <Outlet /> : <Navigate to="/login" replace />;
}

export default Private;
