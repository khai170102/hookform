import { TextField } from "@mui/material";
import { Controller } from "react-hook-form";

interface Iprops {
  name: string;
  label: string;
  form: any;
  autoCompleted?: string;
}
function InputField(props: Iprops) {
  const { name, label, form, autoCompleted } = props;
  return (
    <Controller
      name={name}
      control={form.control}
      render={({ field: { onChange, value } }) => (
        <TextField
          onChange={onChange}
          name={name}
          label={label}
          autoComplete={autoCompleted}
          required
          fullWidth
          id={name}
        />
      )}
    />
  );
}

export default InputField;
