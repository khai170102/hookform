import { Controller } from "react-hook-form";

import { Checkbox } from "@mui/material";
interface iProps {
  name: string;
  form: any;
}

function InputCheckbox(props: iProps) {
  const { name, form } = props;

  return (
    <Controller
      name={name}
      control={form.control}
      render={({ field: { onChange } }) => (
        <Checkbox name={name} onChange={onChange} />
      )}
    />
  );
}

export default InputCheckbox;
