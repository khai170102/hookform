import { Controller } from "react-hook-form";

import { Box, TextField, Typography } from "@mui/material";
interface iProps {
  name: string;
  label: string;
  form: any;
}

function InputDate(props: iProps) {
  const { name, form } = props;

  return (
    <Box>
      <Typography>Date created</Typography>
      <Controller
        name={name}
        control={form.control}
        render={({ field: { onChange } }) => (
          <TextField
            type="date"
            variant="outlined"
            name={name}
            onChange={onChange}
          />
        )}
      />
    </Box>
  );
}

export default InputDate;
