import { Box, MenuItem, Select, Typography } from "@mui/material";
import { Controller } from "react-hook-form";

interface Iprops {
  options: { label: string; value: string }[];
  form: any;
  name: string;
  label: string;
}

function InputSelect(props: Iprops) {
  const { options, form, name } = props;

  return (
    <Box>
      <Typography>Gender: </Typography>
      <Controller
        name={name}
        control={form.control}
        render={({ field }) => (
          <Select
            {...field}
            label="gadsasdasdsa"
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            sx={{ width: 120 }}
          >
            {options.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </Select>
        )}
      />
    </Box>
  );
}

export default InputSelect;
