import {
  Alert,
  AlertTitle,
  AppBar,
  Avatar,
  Box,
  Button,
  Dialog,
  DialogActions,
  IconButton,
  Toolbar,
} from "@mui/material";
import { useEffect, useState } from "react";
import { unwrapResult } from "@reduxjs/toolkit";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import FormRegister from "./components/FormRegister/FormRegister";
import FormLogin from "./components/FormLogin";
import { login, logout, register } from "~/reduxTollkit/userSlice";

function Header() {
  const [open, setOpen] = useState(false);
  const [openLogin, setOpenLogin] = useState(false);
  const [status, setStatus] = useState(false);
  const [showLogout, setShowLogout] = useState(false);
  const [showErroLogin, setShowErroLogin] = useState(false);
  const [showErroRegister, setShowErroRegister] = useState(false);
  const dispatch = useDispatch();
  const loggedInUser = useSelector((state: any) => state.user.current);
  const isLoggedIn = !!loggedInUser.id;
  let navigate = useNavigate();
  const location = useLocation();

  console.log(location.pathname);
  useEffect(() => {
    if (location.pathname == "/login") {
      setOpenLogin(true);
    }
  }, [location]);

  const handleClick = () => {
    setOpen(true);
    // navigate("/login");
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickLogin = () => {
    setOpenLogin(true);
    navigate("/login");
  };

  const handleCloseLogin = () => {
    setOpenLogin(false);
  };

  const handleClickRegister = () => {
    setOpenLogin(false);
    setOpen(true);
  };

  const handleSubmit = async (values: any) => {
    try {
      values.username = values.email;
      const action = register(values);
      const resultAction = await dispatch(action);
      unwrapResult(resultAction);
      setOpen(false);
      setOpenLogin(true);
      setStatus(true);

      setTimeout(() => {
        setStatus(false);
      }, 1000);
    } catch (error) {
      console.log("Failed to register:", error);
      setShowErroRegister(true);

      setTimeout(() => {
        setShowErroRegister(false);
      }, 8000);
    }
  };
  const handleSubmitLogin = async (values: any) => {
    try {
      const action = login(values);
      const resultAction = await dispatch(action);
      navigate("/home");

      unwrapResult(resultAction);
      setOpenLogin(false);
      setStatus(true);
      setTimeout(() => {
        setStatus(false);
      }, 1000);
    } catch (error) {
      console.log("Failed to login:", error);
      setShowErroLogin(true);

      setTimeout(() => {
        setShowErroLogin(false);
      }, 8000);
    }
  };

  const handleClickAvatar = () => {
    setShowLogout(!showLogout);
  };

  const handleLogoutClick = () => {
    const action = logout();

    dispatch(action);
  };

  const goHome = () => {
    navigate("/home");
  };
  const goProduct = () => {
    navigate("/product");
  };
  const goDetail = () => {
    navigate("/detail");
  };

  const goForgotPassword = () => {
    navigate("/forgotpassword");
    setOpenLogin(false);
  };

  return (
    <div>
      {status && (
        <Alert severity="success" className="fixed z-[1000000]">
          <AlertTitle>Success</AlertTitle>
          {open ? `Register Succes ` : `Login Succes `}
        </Alert>
      )}

      <AppBar position="static">
        <Toolbar sx={{ justifyContent: "space-between" }}>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={goHome}
          >
            HookForm
          </IconButton>
          <>
            {isLoggedIn ? (
              <div>
                <a
                  className="px-3 cursor-pointer text-[18px] text-white no-underline	 "
                  onClick={goHome}
                >
                  Home
                </a>
                <a
                  className="px-3 cursor-pointer text-[18px] text-white no-underline"
                  onClick={goProduct}
                >
                  Product
                </a>
                <a
                  className="px-3 cursor-pointer text-[18px] text-white no-underline"
                  onClick={goDetail}
                >
                  Detail
                </a>
              </div>
            ) : null}
          </>
          {isLoggedIn ? (
            <div>
              <div onClick={handleClickAvatar} className="relative">
                <Avatar src="/broken-image.jpg" className="cursor-pointer" />
              </div>
              <a
                href="/"
                className={`${
                  showLogout ? "visible" : "invisible"
                } absolute top-[60px] shadow-sm z-30 border border-solid 
                no-underline	 border-slate-400 cursor-pointer right-[1%] bg-white`}
              >
                <p className="text-black p-3" onClick={handleLogoutClick}>
                  Log out
                </p>
              </a>
            </div>
          ) : (
            <Box>
              <>
                <Button color="inherit" onClick={handleClickLogin}>
                  Login
                </Button>
                <Dialog open={openLogin} onClose={handleCloseLogin}>
                  <FormLogin
                    handleSubmitLogin={handleSubmitLogin}
                    showErroLogin={showErroLogin}
                    setOpen={setOpen}
                  />
                  <DialogActions>
                    <Button onClick={handleClickRegister}>Register</Button>
                    <Button
                      className="cursor-pointer hover:text-blue-300"
                      onClick={goForgotPassword}
                    >
                      ForgotPassword
                    </Button>
                  </DialogActions>
                </Dialog>
              </>

              <Button color="inherit" onClick={handleClick}>
                Register
              </Button>
              <Dialog open={open} onClose={handleClose}>
                <FormRegister
                  handleSubmit={handleSubmit}
                  showErroRegister={showErroRegister}
                />

                <DialogActions>
                  <Button onClick={handleClose}>Cancel</Button>
                </DialogActions>
              </Dialog>
            </Box>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default Header;
