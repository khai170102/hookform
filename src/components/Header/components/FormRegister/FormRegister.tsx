import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";

import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import schemaRegister from "~/components/Validation/RegisterValidation";
import InputField from "~/components/FormControl/InputField";
import InputSelect from "~/components/FormControl/InputSelect";
import InputDate from "~/components/FormControl/InputDate";
import InputFieldPassword from "~/components/FormControl/InputFieldPassword";
import InputCheckbox from "~/components/FormControl/InputCheckbox";
interface iProps {
  handleSubmit: Function;
  showErroRegister: boolean;
}

const options = [
  {
    label: "Man",
    value: "man",
  },
  {
    label: "Wommen",
    value: "wommen",
  },
];

function FormRegister(props: iProps) {
  const { handleSubmit } = props;

  const form = useForm({
    defaultValues: {
      fullName: "",
      email: "",
      password: "",
      retypePassword: "",
      phoneNumber: "",
      gender: "",
      date: "",
      term: false,
    },
    resolver: yupResolver(schemaRegister),
  });

  const onSubmit = (data: any) => {
    if (!onSubmit) {
      return;
    }
    handleSubmit(data);
  };

  return (
    <div>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box
            component="form"
            noValidate
            sx={{ mt: 3 }}
            onSubmit={form.handleSubmit(onSubmit)}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <InputField
                  name="fullName"
                  label="Full name"
                  form={form}
                  autoCompleted="famili-name"
                />
                <Typography sx={{ color: "red" }} variant="body2">
                  {form.formState.errors.fullName?.message}
                </Typography>
              </Grid>

              <Grid item xs={12} sm={6}>
                <InputField
                  name="email"
                  label="Email"
                  form={form}
                  autoCompleted="email"
                />
                <Typography sx={{ color: "red" }} variant="body2">
                  {form.formState.errors.email?.message}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <InputField
                  name="phoneNumber"
                  label="Phonenumber"
                  form={form}
                />
                <Typography sx={{ color: "red" }} variant="body2">
                  {form.formState.errors.phoneNumber?.message}
                </Typography>
              </Grid>

              <Grid item xs={12} sm={6}>
                <InputSelect
                  name="gender"
                  label="gender"
                  form={form}
                  options={options}
                />
                <Typography sx={{ color: "red" }} variant="body2">
                  {form.formState.errors.gender?.message}
                </Typography>
              </Grid>

              <Grid item xs={12} sm={6}>
                <InputDate name="date" label="date" form={form} />
                <Typography sx={{ color: "red" }} variant="body2">
                  {form.formState.errors.date?.message}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <InputFieldPassword
                  name="password"
                  label="Password"
                  form={form}
                />
                <Typography sx={{ color: "red" }} variant="body2">
                  {form.formState.errors.password?.message}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <InputFieldPassword
                  name="retypePassword"
                  label="Retype password"
                  form={form}
                />
                <Typography sx={{ color: "red" }} variant="body2">
                  {form.formState.errors.retypePassword?.message}
                </Typography>
              </Grid>

              <Grid item xs={12}></Grid>
            </Grid>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <InputCheckbox name="term" form={form} />
              <Typography>Confilm</Typography>
            </Box>
            <Typography sx={{ color: "red" }} variant="body2">
              {form.formState.errors.term?.message}
            </Typography>
            {props.showErroRegister ? (
              <Typography sx={{ color: "red" }} variant="body2">
                Email already exists
              </Typography>
            ) : null}

            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign Up
            </Button>

            <Grid container justifyContent="flex-end">
              <Grid item></Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </div>
  );
}

export default FormRegister;
