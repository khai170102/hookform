import Button from "@mui/material/Button";

import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import schemaLogin from "../../../Validation/LoginValueDate";
import InputFieldPassword from "../../../FormControl/InputFieldPassword";
import InputField from "../../../FormControl/InputField";

interface iProps {
  handleSubmitLogin: Function;
  showErroLogin: boolean;
  setOpen: Function;
}
function FormLogin(props: iProps) {
  const { handleSubmitLogin } = props;

  const form = useForm({
    defaultValues: {
      identifier: "",
      password: "",
    },
    resolver: yupResolver(schemaLogin),
  });

  const onSubmitLogin = (data: any) => {
    handleSubmitLogin(data);
  };

  return (
    <div>
      <Container component="main">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            width: "100%",
          }}
        >
          <Typography component="h1" variant="h5">
            Login
          </Typography>
          <Box
            className="w-[100%]"
            component="form"
            noValidate
            sx={{ mt: 3 }}
            onSubmit={form.handleSubmit(onSubmitLogin)}
          >
            <Grid item xs={12} sm={6}>
              <InputField
                name="identifier"
                label="Email"
                form={form}
                autoCompleted="email"
              />
              <Typography sx={{ color: "red" }} variant="body2">
                {form.formState.errors.identifier?.message}
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <InputFieldPassword
                name="password"
                label="Password"
                form={form}
              />
              <Typography sx={{ color: "red" }} variant="body2">
                {form.formState.errors.password?.message}
              </Typography>
            </Grid>

            <Grid item xs={12}></Grid>
            {props.showErroLogin ? (
              <Typography sx={{ color: "red" }} variant="body2">
                Password or email wrong
              </Typography>
            ) : null}

            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign in
            </Button>

            <Grid container justifyContent="flex-end">
              <Grid item></Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </div>
  );
}

export default FormLogin;
