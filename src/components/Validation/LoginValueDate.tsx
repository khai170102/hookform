import * as yup from "yup";

const schemaLogin = yup
  .object({
    identifier: yup
      .string()
      .required("Please enter your email")
      .email("Please enter a valid email address"),
    password: yup
      .string()
      .required("Please enter your password")
      .min(6, "Please enter at least six")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,

        "wrong password format"
      ),
  })
  .required();

export default schemaLogin;
