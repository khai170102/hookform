import * as yup from "yup";

const schemaRegister = yup
  .object({
    fullName: yup.string().required("Please enter your fullname "),

    email: yup
      .string()
      .required("Please enter your email")
      .email("Please enter a valid email address"),
    password: yup
      .string()
      .required("Please enter your password")
      .min(6, "Please enter at least six")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,

        "Passwords Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
      ),

    retypePassword: yup
      .string()
      .required("Please retype your password")
      .oneOf([yup.ref("password")], "Password not match"),
    gender: yup.string().required("Chose gender"),
    phoneNumber: yup
      .string()
      .required("Please enter number")
      .matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/, "Phone number is not valid"),
    date: yup.string().required("please chose date "),
    term: yup
      .boolean()
      .oneOf([true], "You must agree to the terms and conditions to submit"),
  })
  .required();

export default schemaRegister;
