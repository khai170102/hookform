import React from "react";

function Home() {
  return (
    <div className="flex  text-[26px] h-screen font-normal">
      <div className="w-[200px] h-full bg-red-100 ">SiderBar</div>
      <div className=" w-screen flex justify-center">
        <a className="">Home</a>
      </div>
    </div>
  );
}

export default Home;
