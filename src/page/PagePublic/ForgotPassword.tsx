import React from "react";

function ForgotPassword() {
  return (
    <div className="flex justify-center  text-[26px] h-screen font-normal">
      ForgotPassword
    </div>
  );
}

export default ForgotPassword;
