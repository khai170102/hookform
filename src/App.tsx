import { Provider } from "react-redux";
import store from "./reduxTollkit/store";
import RouterLogin from "./router";

function App() {
  return (
    <>
      <Provider store={store}>
        <RouterLogin />
      </Provider>
      ,
    </>
  );
}

export default App;
